#ifndef WIRELESS_NETWORK_H
#define WIRELESS_NETWORK_H

class Transmitter;
class Receiver;
class Channel;

#include <vector>

#include "transmitter.h"
#include "receiver.h"
#include "channel.h"


class WirelessNetwork
{
public:
  const int ktransmitters_number = 4, kreceiver_number = 4;
  const int kseeds_number = 3 * ktransmitters_number + 1; //+1 for channel
  const int kseed_save_step = 100000;
  WirelessNetwork(int seed_);
  ~WirelessNetwork();

  int GetInitialPackageNumber() { return initial_package_number; }
  void SetInitialPackageNumber(int time) { initial_package_number = time; }

  double GetLambda() { return lambda; }
  void SetLambda(double lambda_) { lambda = lambda_; }

  double GetNumberOfAllRetransmission() { return number_of_all_retransmission; }
  void SetNumberOfAllRetransmision(int number) { number_of_all_retransmission += number; }

  double WholeAveragePackageErrorRate(size_t time);  //average of all transmitters packet error rate
  double WholeAverageRetransmissionNumber(size_t time);

  int GetAllTransmissionsNumber();
  int GetAllPackagesSent();

  size_t GetPackageLifeAverage();
  void SetPackageLifeAverage(size_t time) { package_life_average += time; }

  size_t GetPackageWaitingTime();
  void SetPackageWaitingTime(size_t time) { package_waiting_time += time; }

  int GetPackagesSentSuccessfully();
  void SetPackagesSentSuccessfully();


  std::vector<Transmitter*>* GetTransmitters();
  std::vector<Receiver*>* GetReceivers();
  std::vector<int>* GetSeeds();
  Channel* GetChannel();

private:
  std::vector<Transmitter*>transmitters; // Vector of transmitters
  std::vector<Receiver*>receivers; // Vector of receivers
  std::vector<int>seeds;
  Channel* channel = nullptr;
  int initial_package_number = 0;  //time of initial phase
  double lambda = 0.0337;
  double number_of_all_retransmission = 0.0;
  size_t package_life_average = 0;
  size_t package_waiting_time = 0;
  Generator* seed_generator = nullptr;
  int seed = 0;
  int packages_sent_successfully = 0; //for initial phase
};

#endif // WIRELESS_NETWORK_H

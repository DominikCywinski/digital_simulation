#include "package.h"

Package::Package(int id,size_t ctpk)
{
  package_id = id;
  ctp=ctpk;
}

Package::~Package()
{
}

int Package::GetPackageId()
{
  return package_id;
}

int Package::GetRetransmissionNumber()
{
  return retransmission_number;
}

void Package::SetRetransmissionNumber()
{
  retransmission_number++;
}

size_t Package::GetCtpTime()
{
  return ctp;
}

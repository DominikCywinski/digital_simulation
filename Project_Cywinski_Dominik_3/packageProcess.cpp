#include "packageProcess.h"
#include "wireless_network.h"
#include "channel.h"
#include "transmitter.h"
#include "logger.h"
#include "simulator.h"

#include <fstream>
#include <cstdlib>
#include <iostream>
#include <string>

PackageProcess::PackageProcess(WirelessNetwork* wireless_network_ptr, size_t time, Logger* logger, int tx_id, int pack_id, Agenda* agenda)
{
  activation_time = time;
  appear_time = activation_time;
  logger_ = logger;
  transmitter_id = tx_id;
  wireless_network = wireless_network_ptr;
  package_id = pack_id;
  agenda_ = agenda;
}

PackageProcess::~PackageProcess()
{
}

bool PackageProcess::IsTerminated()
{
  return terminated;
}

void PackageProcess::SetTerminated()
{
  terminated = true;
}

void PackageProcess::Activate(size_t time, bool relative)
{
  if (relative)
  {
    activation_time += time;
  }
  else
  {
    activation_time = time;
  }
  agenda_->push(this);
}

void PackageProcess::Execute()
{
  bool active = true;

  while (active)
  {
    switch (state)
    {
    case State::Generate:

      wireless_network->GetTransmitters()->at(transmitter_id)->GeneratePackage(); //generate new package+add to queue
      {
        auto new_process = new PackageProcess(wireless_network, activation_time, logger_, transmitter_id, package_id + 1, agenda_);
        new_process->Activate(wireless_network->GetTransmitters()->at(0)->GetExpGenerator()->RndExp(wireless_network->GetLambda()) * 10);  //plan new process  *10->1ms
      }

      if (!wireless_network->GetTransmitters()->at(transmitter_id)->GetTransmitterBusy()) //if transmitter isn't busy
      {
        wireless_network->GetTransmitters()->at(transmitter_id)->SetTransmitterBusy(true); //this transmitter is busy now
        if (wireless_network->GetPackagesSentSuccessfully() > (wireless_network->GetInitialPackageNumber()))
          wireless_network->SetPackageWaitingTime(activation_time - appear_time); //get time of package waiting

        state = State::ChannelCheck;
        active = true;
      }
      else
      {
        state = State::Generate;
        active = false;
      }

      break;

    case State::ChannelCheck:

//      logger_->Info("\nTRANSMITTER  " + std::to_string(transmitter_id) + "  Channel listenning...");

      if (wireless_network->GetChannel()->IsChannelEmpty()) // if channel is free
      {
        if (wireless_network->GetTransmitters()->at(transmitter_id)->GetDifsTime() > 50) //if channel is free longer than DIFS=5ms 
        {
          Activate(0);  //wait for others transmitters.

          state = State::Transmission;
          active = false;
        }
        else
        {
          wireless_network->GetTransmitters()->at(transmitter_id)->SetDifsTime(wireless_network->GetTransmitters()->at(transmitter_id)->GetDifsTime() + 5); // increase DIFS by 0.5 ms
          Activate(5);  //wait 0.5 ms

          state = State::ChannelCheck;
          active = false;
        }
      }
      else
      {
//        logger_->Info("\nTRANSMITTER  " + std::to_string(transmitter_id) + "  Channel is busy");
        wireless_network->GetTransmitters()->at(transmitter_id)->SetDifsTime(0);
        Activate(5);  //wait 0.5 ms

        state = State::ChannelCheck;
        active = false;
      }

      break;

    case State::Transmission:

//      logger_->Info("\nTRANSMITTER  " + std::to_string(transmitter_id) + "  start transmission package (ID):  " + std::to_string(wireless_network->GetTransmitters()->at(transmitter_id)->GetPackages()->front()->GetPackageId()));

      for (int i = 0; i < wireless_network->GetTransmitters()->size(); ++i)
      {
        wireless_network->GetTransmitters()->at(i)->SetDifsTime(0); // set every DIFS=0 ms for next transmission.
      }

      if (!wireless_network->GetTransmitters()->at(transmitter_id)->GetPackageStatus()) //if not sent
      {
        wireless_network->GetChannel()->AddPackages(wireless_network->GetTransmitters()->at(transmitter_id)->GetPackages()->front()); //add package to channel
        wireless_network->GetTransmitters()->at(transmitter_id)->SetPackageStatus(true);  //set package is in transmission
        Activate(0);

        state = State::CheckCollision;
        active = false;
      }
      else
      {
        active = true;
        state = State::CheckCollision;
      }

      break;

    case State::CheckCollision:

      wireless_network->GetChannel()->SetTerStatus();  //probability of TER ERROR -> 0.8
      wireless_network->GetTransmitters()->at(transmitter_id)->SetPackageStatus(false); //set for next transmission

      if (wireless_network->GetChannel()->GetPackagesInChannel()->size() == 1 && wireless_network->GetChannel()->GetTerStatus() == false)
      {
//        logger_->Info("\nTRANSMITTER  " + std::to_string(transmitter_id) + "  No collision in channel");
        Activate(wireless_network->GetTransmitters()->at(transmitter_id)->GetPackages()->front()->GetCtpTime()); //add time of transmission

        state = State::GenerateAck;
        active = false;
      }
      else
      {
//        logger_->Error("\nTRANSMITTER  " + std::to_string(transmitter_id) + "  Detected collision!");

        while (!wireless_network->GetChannel()->IsChannelEmpty()) //while channel isn't empty
        {
          wireless_network->GetChannel()->DeleteFromBuffer(); //delete all packages from buffer in channel         
        }
        //Retransmission
        if (wireless_network->GetTransmitters()->at(transmitter_id)->GetPackages()->front()->GetRetransmissionNumber() < max_retransmission_number) //check number of retransmissions
        {
          wireless_network->GetTransmitters()->at(transmitter_id)->GetPackages()->front()->SetRetransmissionNumber(); //increase number of retransmissions

          Activate(wireless_network->GetTransmitters()->at(transmitter_id)->GetPackages()->front()->GetCtpTime() * //wait time to do retransmission
            wireless_network->GetTransmitters()->at(transmitter_id)->GetRGenerator()->Rand(0, std::pow(2, (wireless_network->
              GetTransmitters()->at(transmitter_id)->GetPackages()->front()->GetRetransmissionNumber())) - 1) * 10
            + wireless_network->GetReceivers()->at(transmitter_id)->GetCtiz());                                    //+time when ACK should appear         

//          logger_->Info("\nTRANSMITTER  " + std::to_string(transmitter_id) + "  Retransmission Package (ID):  "
//            + std::to_string(wireless_network->GetTransmitters()->at(transmitter_id)->GetPackages()->front()->GetPackageId()));

          state = State::ChannelCheck;
          active = false;
        }
        else
        {
          if (activation_time > (wireless_network->GetInitialPackageNumber()))
            wireless_network->GetTransmitters()->at(transmitter_id)->SetLostPackagesNumber(); //increase number of lost packages

//          logger_->Info("\nTRANSMITTER  " + std::to_string(transmitter_id) + "  lost package (ID):  " + std::to_string(wireless_network->GetTransmitters()->at(transmitter_id)->GetPackages()->front()->GetPackageId()));

          state = State::Removal;
          active = true;
        }
      }
      break;

    case State::GenerateAck:

      if (!wireless_network->GetReceivers()->at(transmitter_id)->GetAckSent())
      {
//        logger_->Info("\nRECEIVER  " + std::to_string(transmitter_id) + "  Send ACK");
        wireless_network->GetReceivers()->at(transmitter_id)->SetAckSent(true); //set ACK as sent
        Activate(wireless_network->GetReceivers()->at(transmitter_id)->GetCtiz()); //wait time of ACK transmission

        active = false;
      }
      else
      {
        wireless_network->GetTransmitters()->at(transmitter_id)->SetAckStatus(true);  //ACK received

        state = State::CheckAckStatus;
        active = true;
      }

      break;

    case State::CheckAckStatus:

      if (wireless_network->GetTransmitters()->at(transmitter_id)->GetAckStatus()) //if ack received 
      {
        wireless_network->SetPackagesSentSuccessfully();
        if (wireless_network->GetPackagesSentSuccessfully() > (wireless_network->GetInitialPackageNumber()))
        {
          wireless_network->GetTransmitters()->at(transmitter_id)->SetPackagesSentSuccessfullyNumber(); //increase number of sent packages
          wireless_network->SetPackageLifeAverage(activation_time - appear_time); //save time of life package (successfull transmission)
        }
        wireless_network->GetTransmitters()->at(transmitter_id)->SetAckStatus(false); //set false for next transmission
        wireless_network->GetReceivers()->at(transmitter_id)->SetAckSent(false);

//        logger_->Info("\nTRANSMITTER  " + std::to_string(transmitter_id) + "  Package (ID): "
//          + std::to_string(wireless_network->GetTransmitters()->at(transmitter_id)->GetPackages()->front()->GetPackageId()) + " Received correctly");

        state = State::Removal;
        active = true;
      }
      else//  In case of unexpected error... But never done in my simulations.
      {
        logger_->Error("\nTRANSMITTER  " + std::to_string(transmitter_id) + "  Package (ID): "
          + std::to_string(wireless_network->GetTransmitters()->at(transmitter_id)->GetPackages()->front()->GetPackageId()) + "  NOT RECEIVED!");

        //Retransmission
        if (wireless_network->GetTransmitters()->at(transmitter_id)->GetPackages()->front()->GetRetransmissionNumber() <= 10) //check number of retransmissions
        {
          wireless_network->GetTransmitters()->at(transmitter_id)->GetPackages()->front()->SetRetransmissionNumber(); //increase number of retransmissions

          Activate(wireless_network->GetTransmitters()->at(transmitter_id)->GetPackages()->front()->GetCtpTime() * //wait time to do retransmission
            wireless_network->GetTransmitters()->at(transmitter_id)->GetRGenerator()->Rand(0, std::pow(2, (wireless_network->
              GetTransmitters()->at(transmitter_id)->GetPackages()->front()->GetRetransmissionNumber())) - 1) * 10);

          logger_->Info("\nTRANSMITTER  " + std::to_string(transmitter_id) + "  Retransmission Package (ID): "
            + std::to_string(wireless_network->GetTransmitters()->at(transmitter_id)->GetPackages()->front()->GetPackageId()));

          state = State::ChannelCheck;
          active = false;
        }
        else
        {
          state = State::Removal;
          active = true;
        }
      }

      break;

    case State::Removal:
      //logger_->Info("\nAverage Package Error rate of all transmitters:  " + std::to_string(wireless_network->WholeAveragePackageErrorRate(activation_time)));

//      logger_->Info("\nTRANSMITTER  " + std::to_string(transmitter_id) + "  Package (ID):  "
//        + std::to_string(wireless_network->GetTransmitters()->at(transmitter_id)->GetPackages()->front()->GetPackageId()) + "  Deleted.");

      if (wireless_network->GetPackagesSentSuccessfully() > (wireless_network->GetInitialPackageNumber()))
      {
        wireless_network->SetNumberOfAllRetransmision(wireless_network->GetTransmitters()->at(transmitter_id)->GetPackages()->front()->GetRetransmissionNumber());//save number of retransmission
        wireless_network->WholeAverageRetransmissionNumber(activation_time);
      }
      wireless_network->GetTransmitters()->at(transmitter_id)->EndTransmission(); //delete package from transmitter buffer

      while (!wireless_network->GetChannel()->IsChannelEmpty())
      {
        wireless_network->GetChannel()->DeleteFromBuffer(); //delete package from channel
      }
//      logger_->Info("\nTRANSMITTER  " + std::to_string(transmitter_id) + "  Checking buffer");

      if (!wireless_network->GetTransmitters()->at(transmitter_id)->IsBufferEmpty()) //if any package in transmitter buffer
      {
        {
          auto new_process = new PackageProcess(wireless_network, activation_time, logger_, transmitter_id,
            wireless_network->GetTransmitters()->at(transmitter_id)->GetPackages()->front()->GetPackageId(), agenda_);

          new_process->Activate(activation_time, false); //new process in current time.
        }
      }
      SetTerminated(); //delete process from system
      wireless_network->GetTransmitters()->at(transmitter_id)->SetTransmitterBusy(false); //transmitter isn't busy now

      active = false;
      break;
    default:
      logger_->Error("\n\t\t UNEXPECTED ERROR!!!\n");

      break;
    }
  }
}


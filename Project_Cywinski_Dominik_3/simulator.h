#ifndef SIMULATOR_H_
#define SIMULATOR_H_

#include "wireless_network.h"
#include "logger.h"
#include "package.h"

class Simulator
{
public:
  explicit Simulator(WirelessNetwork* wireless_network_);

  void Run(int time);
  void StepByStep();
  bool Mode(int mode);

private:
  WirelessNetwork* wireless_network = nullptr;
  size_t clock;
  Logger* logger = nullptr;
};

#endif
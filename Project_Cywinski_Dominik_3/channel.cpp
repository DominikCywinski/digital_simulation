#include "channel.h"
#include "generator.h"
#include <iostream>

Channel::Channel(int seed)
{
  ter_generator = new Generator(seed);
}

Channel::~Channel()
{
}

bool Channel::GetChannelError()
{
  return channel_error;
}

void Channel::SetChannelError(bool channel_er)
{
  channel_error = channel_er;
}

bool Channel::GetTerStatus()
{
  return ter_status;
}

void Channel::SetTerStatus()
{
  ////20% probability to appear TER Error
  if (GetTerGenerator()->RndZeroOne(ter_probability))
    ter_status = false;
  else
    ter_status = true;
}

std::vector<Package*>* Channel::GetPackagesInChannel()
{
  return &packages_in_channel;
}

void Channel::AddPackages(Package* npackage)
{
  packages_in_channel.push_back(npackage);
}

bool Channel::IsChannelEmpty()
{
  return packages_in_channel.empty();
}

void Channel::DeleteFromBuffer()
{
  packages_in_channel.pop_back();
}

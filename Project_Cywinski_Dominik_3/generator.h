#ifndef RANDOM_GENERATOR_H_
#define RANDOM_GENERATOR_H_

#include <string>

class Generator
{
public:
  Generator(int kernel);
  virtual ~Generator();

  // Draws number between <0,1>
  double Rand();
  int Rand(double min, double max);
  int RndExp(double lambda);
  int RndZeroOne(double p);
  int GetKernel();

  int get_kernel() { return kernel; };
private:
  int kernel;
  const double kM = 2147483647.0;
  static const int kA = 16807;
  static const int kQ = 127773;
  static const int kR = 2836;
};


#endif /* RANDOM_GENERATOR_H_ */
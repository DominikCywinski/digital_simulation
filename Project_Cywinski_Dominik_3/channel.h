#ifndef CHANNEL_H
#define CHANNEL_H

#include <vector>

#include "generator.h"
#include "package.h"

//class to manage channel.
class Channel
{
public:
  Channel(int seed);
  ~Channel();

  bool GetChannelError();
  void SetChannelError(bool channel_er);

  bool GetTerStatus();
  void SetTerStatus();

  std::vector<Package*>* GetPackagesInChannel();
  void AddPackages(Package* npackage);
  bool IsChannelEmpty();
  void DeleteFromBuffer();
  Generator* GetTerGenerator() { return ter_generator; }

private:
  std::vector<Package*>packages_in_channel; //packages located in channel
  bool channel_error = false; //any error during transmission? true-error 
  bool ter_status = false; //true-ter error
  Generator* ter_generator = nullptr;
  double ter_probability = 0.8; //probability of TER error appear = 0.2
};

#endif 
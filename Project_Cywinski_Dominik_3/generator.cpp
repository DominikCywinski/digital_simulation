#include "generator.h"
#include <cmath>

Generator::Generator(int kernel) : kernel(kernel)
{
}

Generator::~Generator()
{
}

double Generator::Rand()
{
  int h = kernel / kQ;
  kernel = kA * (kernel - kQ * h) - kR * h;
  if (kernel < 0)
    kernel = kernel + static_cast<int>(kM);
  return kernel / kM;
}

int Generator::Rand(double min, double max)
{
  return Rand() * (max - min) + min;
}

int Generator::RndExp(double lambda)
{
  auto k = Rand();
  return -(1.0 / lambda) * log(k);
}

int Generator::RndZeroOne(double p)
{
  auto k = Rand();
  if (k < p)
    return 1;
  else
    return 0;
}

int Generator::GetKernel()
{
  return kernel;
}
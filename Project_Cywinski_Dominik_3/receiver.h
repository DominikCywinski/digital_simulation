#ifndef RECEIVER_H
#define RECEIVER_H

class Receiver
{
public:
  Receiver(int id);
  ~Receiver();

  //get set
  int GetReceiverId() { return receiver_id; }
  void SetReceiverId(int rec_id) { receiver_id = rec_id; }

  bool GetPackageReceivedCorrectly() { return package_received_correctly; }
  void SetPackageReceivedCorrectly(int package_received) { package_received_correctly = package_received; }

  bool GetAckSent() { return ack_sent; }
  void SetAckSent(bool status) { ack_sent = status; }

  size_t GetCtiz();

private:
  int receiver_id;
  bool package_received_correctly = false; //true-correct, false-error
  bool ack_sent = false; //true-> ack sent 
  size_t ctiz = 10; //time of ACK transmission
};
#endif
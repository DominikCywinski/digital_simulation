#ifndef TRANSMITTER_H
#define TRANSMITTER_H

#include<queue>

#include "package.h"
#include "generator.h"

class Transmitter
{
public:
  Transmitter(int id, int uniform_seed, int exp_seed, int r_seed);
  ~Transmitter();

  //get set
  int GetTransmitterId();
  void SetTransmitterId(int trans_id);

  bool GetAckStatus();
  void SetAckStatus(bool ack_status);

  bool GetPackageStatus();
  void SetPackageStatus(bool package_status);

  size_t GetDifsTime();
  void SetDifsTime(size_t dif);

  size_t GetCgpTime();

  bool GetTransmitterBusy() { return busy; }
  void SetTransmitterBusy(bool status) { busy = status; }

  int GetPackagesSentSuccessfullyNumber() { return packages_sent_successfully_number; }
  void SetPackagesSentSuccessfullyNumber() { ++packages_sent_successfully_number; } //increase number of packages

  int GetLostPackagesNumber() { return lost_packages_number; }
  void SetLostPackagesNumber() { ++lost_packages_number; } //increase number of lost packages

  double GetAveragePackageErrorRate();
  //void SetAveragePackageErrorRate(double average) { average_package_error_rate = average; }

  Generator* GetUniformGenerator() { return uniform_generator; }
  Generator* GetExpGenerator() { return exp_generator; }
  Generator* GetRGenerator() { return r_generator; }

  void GeneratePackage();
  void StartTransmission(Package* package);
  void EndTransmission();
  bool IsBufferEmpty();


  std::queue<Package*>* GetPackages();

private:
  std::queue<Package*>packages; //list(queue) of packages
  int transmitter_id;
  size_t difs_time = 0; //time of listening
  size_t cgp = 1; //random time to generate package
  bool ack_get = false; //true-ACK received
  bool package_sent = false; //true-package in transmission
  bool busy = false; //true-transmitter is transmitting
  double packages_sent_successfully_number = 0.00; //number of packages sent in current transmitter 
  double lost_packages_number = 0.00; //number of lost packages
  double average_package_error_rate = 0.00;
  Generator* uniform_generator = nullptr;
  Generator* exp_generator = nullptr;
  Generator* r_generator = nullptr;

};

#endif
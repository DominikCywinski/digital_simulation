#ifndef PACKAGE_PROCESS_H_
#define PACKAGE_PROCESS_H_

#include<functional>

#include "logger.h"
#include "wireless_network.h"
#include "package.h"
#include "transmitter.h"
#include "receiver.h"
#include "channel.h"

//class represents process in the system.
class Transmitter;
class WirelessNetwork;
class Channel;

class PackageProcess
{
public:
  typedef std::priority_queue<PackageProcess*, std::vector<PackageProcess*>, std::function<bool(PackageProcess*, PackageProcess*)>> Agenda;
  enum class State { Generate, ChannelCheck, Transmission, CheckCollision, GenerateAck, CheckAckStatus, Removal }; //seven phases.

  PackageProcess(WirelessNetwork* wieless_network_ptr, size_t time, Logger* logger, int tx_id, int pack_id, Agenda* agenda);
  ~PackageProcess();

  size_t GetActivationTime() { return activation_time; }

  bool IsTerminated();
  void SetTerminated();

  bool GetIsTransmitted() { return is_transmitted; }
  void SetIsTransmitted(bool status) { is_transmitted = status; }

  bool GetCollision() { return collision; }
  void SetCollision(bool collision_) { collision = collision_; }

  void Execute();
  void Activate(size_t time, bool relative = true);
  void Retransmission();

private:
  State state = State::Generate; //start from generate package
  Logger* logger_ = nullptr;
  size_t activation_time;
  size_t appear_time;
  WirelessNetwork* wireless_network = nullptr;
  Agenda* agenda_ = nullptr;
  size_t generate_packet_max_time = 100;
  size_t transmission_max_time = 100;
  bool terminated = false;
  int transmitter_id;
  int package_id;
  bool is_transmitted = false; //true-> package in transmission
  bool collision = false; //true->collision detected
  int max_retransmission_number = 5; //10 is too high
};

#endif
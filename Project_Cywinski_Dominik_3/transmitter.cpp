#include "transmitter.h"
#include "generator.h"

#include <iostream>

Transmitter::Transmitter(int id, int uniform_seed, int exp_seed, int r_seed)
{
  transmitter_id = id;
  uniform_generator = new Generator(uniform_seed);
  exp_generator = new Generator(exp_seed);
  r_generator = new Generator(r_seed);
}

Transmitter::~Transmitter()
{
}

double Transmitter::GetAveragePackageErrorRate()
{
  if (packages_sent_successfully_number != 0)
    average_package_error_rate = lost_packages_number / packages_sent_successfully_number;
  else
    average_package_error_rate = 0;

  return average_package_error_rate;
}

void Transmitter::GeneratePackage()
{
  static size_t id = 0;
  auto package = new Package(id, GetUniformGenerator()->Rand(1, 10) * 10); //*10 -> 1 ms
  ++id;
  packages.push(package);

//  std::cout << "TRANSMITTER  " << transmitter_id << "  package ID: " << package->GetPackageId() << " created.\n" << std::endl;
}

int Transmitter::GetTransmitterId()
{
  return transmitter_id;
}

void Transmitter::SetTransmitterId(int trans_id)
{
  transmitter_id = trans_id;
}

bool Transmitter::GetAckStatus()
{
  return ack_get;
}

void Transmitter::SetAckStatus(bool ack_status)
{
  ack_get = ack_status;
}

bool Transmitter::GetPackageStatus()
{
  return package_sent;
}

void Transmitter::SetPackageStatus(bool package_status)
{
  package_sent = package_status;
}

size_t Transmitter::GetDifsTime()
{
  return difs_time;
}

void Transmitter::SetDifsTime(size_t dif)
{
  difs_time = dif;
}

size_t Transmitter::GetCgpTime()
{
  return cgp;
}

void Transmitter::StartTransmission(Package* package)
{

}

void Transmitter::EndTransmission()
{
  packages.pop();
}

bool Transmitter::IsBufferEmpty()
{
  return packages.empty();
}

std::queue<Package*>* Transmitter::GetPackages()
{
  return &packages;
}

#include "simulator.h"
#include "packageProcess.h"
#include "package.h"

#include <iostream>


Simulator::Simulator(WirelessNetwork* wireless_network_)
{
  wireless_network = wireless_network_;
}


void Simulator::StepByStep()
{
  char key;
  std::cout << "\nPress key to continue" << std::endl;
  key = getchar();
}

bool Simulator::Mode(int mode)
{
  if (mode == 1)
    return true;
  else if (mode == 2)
    return false;
}


void Simulator::Run(int time)
{
  clock = 0;
  int mode;

  auto logger = Logger();
  logger.set_level(Logger::Level::Info);

  std::cout << "START OF SIMULATION...\n\nSelect simulation mode\n  1.Step by step\n  2.Step over" << std::endl;

  std::cin >> mode;

  auto cmp = [](PackageProcess* left, PackageProcess* right) { return left->GetActivationTime() > right->GetActivationTime(); };
  PackageProcess::Agenda agenda(cmp);

  //create first package processes 
  agenda.push(new PackageProcess(wireless_network, wireless_network->GetTransmitters()->at(0)->GetExpGenerator()->RndExp(wireless_network->GetLambda()) * 10, &logger, 0, 0, &agenda));
  agenda.push(new PackageProcess(wireless_network, wireless_network->GetTransmitters()->at(1)->GetExpGenerator()->RndExp(wireless_network->GetLambda()) * 10, &logger, 1, 0, &agenda));
  agenda.push(new PackageProcess(wireless_network, wireless_network->GetTransmitters()->at(2)->GetExpGenerator()->RndExp(wireless_network->GetLambda()) * 10, &logger, 2, 0, &agenda));
  agenda.push(new PackageProcess(wireless_network, wireless_network->GetTransmitters()->at(3)->GetExpGenerator()->RndExp(wireless_network->GetLambda()) * 10, &logger, 3, 0, &agenda));

  int temp_clock = agenda.top()->GetActivationTime() + 1;  //to show only one simulation time and go into ''if'' from line : 59

  while (clock < static_cast<size_t>(time) && (!agenda.empty() == true))
  {
    PackageProcess* process = agenda.top();
    clock = process->GetActivationTime();
    agenda.pop();

    if (temp_clock != clock) //to avoid repetition of showing simulation time
    {
      temp_clock = clock;
//      std::cout << "\n\t\tSimulation Time: " << clock << "\n" << std::endl;
    }
    process->Execute();

    if (process->IsTerminated())
    {
      delete process;
      continue;
    }

    if (Mode(mode) == true)
    {
      StepByStep();
    }
  }
  //show data.
  std::cout << "\t\t\nSIMULATION RESULTS...\n" << std::endl;

  //if (wireless_network->GetAllTransmissionsNumber())
  std::cout << "Number of Packages sent successfully:  " << wireless_network->GetAllTransmissionsNumber() << std::endl;
  std::cout << "Number of all retransmissions:  " << wireless_network->GetNumberOfAllRetransmission() << std::endl;

  if (wireless_network->GetAllPackagesSent())
    std::cout << "\nAverage of package retransmission:  " << wireless_network->GetNumberOfAllRetransmission() / wireless_network->GetAllPackagesSent() << std::endl;

  std::cout << "\nAverage Package Error Rate:" << std::endl;

  int max_average = -1;
  int max_index = 0;

  for (int i = 0; i < wireless_network->ktransmitters_number; ++i)
  {
    std::cout << "TRANSMITTER:  " << i << " = " << wireless_network->GetTransmitters()->at(i)->GetAveragePackageErrorRate() << std::endl;
    if (wireless_network->GetTransmitters()->at(i)->GetAveragePackageErrorRate() > max_average)
    {
      max_average = wireless_network->GetTransmitters()->at(i)->GetAveragePackageErrorRate();
      max_index = i;
    }
  }
  std::cout << "MAX IN TRANSMITTER:  " << max_index << " = " << wireless_network->GetTransmitters()->at(max_index)->GetAveragePackageErrorRate() << std::endl;
  std::cout << "IN SIMULATION:  " << wireless_network->WholeAveragePackageErrorRate(clock) << std::endl;
  std::cout << "Successfully Packages transmission:  " << ((double)(wireless_network->GetAllTransmissionsNumber())) / (((double)(time)-
    (double)(wireless_network->GetInitialPackageNumber())) / 10000) << "  per second" << std::endl;
  std::cout << "\nAverage of package life:  " << wireless_network->GetPackageLifeAverage() << std::endl;
  std::cout << "\nAverage of package waiting:  " << wireless_network->GetPackageWaitingTime() << std::endl;
}
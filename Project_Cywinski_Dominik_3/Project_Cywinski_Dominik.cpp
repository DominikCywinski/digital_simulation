#include <iostream>
#include <time.h>
#include <fstream>
#include <vector>
#include <fstream>
#include <iterator>

#include "wireless_network.h"
#include "simulator.h"
#include "generator.h"

int main()
{
  //create txt's to save data.
  std::ofstream SaveWholeAveragePackageErrorRate("WholeAveragePackageErrorRate.txt");
  std::ofstream SaveRetransmissionAverage("RetranssmissionAverage.txt");
  std::ofstream SaveUniformGenerator("UniformGenerator.txt");

  Logger logger = Logger();
  logger.set_level(Logger::Level::Debug);

  int initial_package_number = 300; //0
  int simulation_time = 200000; //0
  int set_seed = 0;
  double lambda = 0.0337;

  std::cout << "Enter Time of Simulation (Recommended: more than 150000)" << std::endl;
  std::cin >> simulation_time;
  std::cout << "Enter the package number after which the simulation will start collecting data (Recommended: 300)." << std::endl;
  std::cin >> initial_package_number;
  std::cout << "Enter number of simulation (1-10)." << std::endl;
  std::cin >> set_seed;
  std::cout << "Enter lambda (Recommended: 0.0337)." << std::endl;
  std::cin >> lambda;

  auto w_network = new WirelessNetwork(100 * set_seed);
  auto simulator = new Simulator(w_network);

  w_network->SetInitialPackageNumber(initial_package_number);
  w_network->SetLambda(lambda);

  simulator->Run(simulation_time);

  delete simulator;
  delete w_network;
}
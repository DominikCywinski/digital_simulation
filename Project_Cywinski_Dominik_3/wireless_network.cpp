#include "wireless_network.h"
#include "generator.h"
#include "channel.h"
#include <fstream>
#include <iostream>
#include <string>

WirelessNetwork::WirelessNetwork(int seed_)
{
  seed = seed_;
  seed_generator = new Generator(seed);

  for (int i = 0; i < kseed_save_step * kseeds_number; ++i)
  {
    seed_generator->Rand();

    if (i % kseed_save_step == 0)
      seeds.push_back(seed_generator->GetKernel());
  }

  for (int i = 0; i < ktransmitters_number; ++i)
  {
    int uniform_seed = seeds.back();
    seeds.pop_back();
    int exp_seed = seeds.back();
    seeds.pop_back();
    int r_seed = seeds.back();
    seeds.pop_back();

    auto transmitter = new Transmitter(i, uniform_seed, exp_seed, r_seed);
    transmitters.push_back(transmitter);

    auto receiver = new Receiver(i);
    receivers.push_back(receiver);
  }
  int seed_temp = seeds.back();
  channel = new Channel(seed_temp);
  seeds.pop_back();
}

WirelessNetwork::~WirelessNetwork()
{
}

double WirelessNetwork::WholeAveragePackageErrorRate(size_t time)
{
  double average = 0;

  for (int i = 0; i < ktransmitters_number; ++i)
  {
    average += GetTransmitters()->at(i)->GetAveragePackageErrorRate();
  }
  average = average / (GetTransmitters()->size());

  std::ofstream SaveAveragePackageErrorRate("WholeAveragePackageErrorRate.txt", std::ios_base::app);
  SaveAveragePackageErrorRate << "time  " + std::to_string(time) +
    "Average Package Error Rate of all transmitters:  " << average << std::endl;

  SaveAveragePackageErrorRate.close();

  return average;
  //TO DO:: save to file.
}

double WirelessNetwork::WholeAverageRetransmissionNumber(size_t time)
{
  std::ofstream SaveRetransmissionAverage("RetranssmissionAverage.txt", std::ios_base::app);
  SaveRetransmissionAverage << //std::to_string(GetAllTransmissionsNumber()) + "\t" <<
  GetNumberOfAllRetransmission() / GetAllPackagesSent() << std::endl;

  SaveRetransmissionAverage.close();

  return 0.0;
}

int WirelessNetwork::GetAllTransmissionsNumber()
{
  int number = 0;

  for (int i = 0; i < ktransmitters_number; ++i)
  {
    number += GetTransmitters()->at(i)->GetPackagesSentSuccessfullyNumber();
  }
  return number;
}

int WirelessNetwork::GetAllPackagesSent()
{
  int number = 0;

  for (int i = 0; i < ktransmitters_number; ++i)
  {
    number += GetTransmitters()->at(i)->GetPackagesSentSuccessfullyNumber() + GetTransmitters()->at(i)->GetLostPackagesNumber();
  }
  return number;
}

size_t WirelessNetwork::GetPackageLifeAverage()
{
  if (GetAllTransmissionsNumber() != 0)
    return package_life_average / GetAllTransmissionsNumber();
  else
    return 0;
}

size_t WirelessNetwork::GetPackageWaitingTime()
{
  if (GetAllPackagesSent() != 0)
    return package_waiting_time / GetAllPackagesSent();
  else
    return 0;
}

int WirelessNetwork::GetPackagesSentSuccessfully()
{
  return packages_sent_successfully;
}

void WirelessNetwork::SetPackagesSentSuccessfully()
{
  ++packages_sent_successfully;
}

std::vector<Transmitter*>* WirelessNetwork::GetTransmitters()
{
  return &transmitters;
}

std::vector<Receiver*>* WirelessNetwork::GetReceivers()
{
  return &receivers;
}

std::vector<int>* WirelessNetwork::GetSeeds()
{
  return &seeds;
}

Channel* WirelessNetwork::GetChannel()
{
  return channel;
}


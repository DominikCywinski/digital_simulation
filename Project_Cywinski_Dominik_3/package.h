#ifndef PACKAGE_H
#define PACKAGE_H

#include <cstdlib>

class Package
{
public:
  Package(int id,size_t ctpk);
  ~Package();

  //get set
  int GetPackageId();
  int GetRetransmissionNumber();
  void SetRetransmissionNumber(); //increasing number of retransmission
  size_t GetCtpTime();

private:
  int retransmission_number = 0; //number of package retransmissions
  int package_id = 0;
  size_t ctp; //time of package transmission
};

#endif 
